openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ./nginx-ssl.key -out ./nginx-ssl.crt
openssl dhparam -out ./nginx-ssl.pem 2048
